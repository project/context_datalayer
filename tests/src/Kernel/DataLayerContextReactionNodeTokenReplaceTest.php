<?php

namespace Drupal\Tests\context_datalayer\Kernel;

use Drupal\context_datalayer\Plugin\ContextReaction\DataLayer;
use Drupal\node\Entity\Node;
use Drupal\node\Entity\NodeType;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\Core\Routing\CurrentRouteMatch;

/**
 * DataLayer Context Reaction Node Token Replace Test.
 *
 * @group context_datalayer
 */
class DataLayerContextReactionNodeTokenReplaceTest extends EntityKernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['node', 'filter'];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->installConfig(['filter', 'node']);

    $node_type = NodeType::create(['type' => 'article', 'name' => 'Article']);
    $node_type->save();
    node_add_body_field($node_type);
  }

  /**
   * Test Node Token Replacement.
   *
   * @dataProvider providerTokenConfigScenarios
   */
  public function testNodeTokenReplacement($configuration, $expected) {
    $url_options = [
      'absolute' => TRUE,
      'language' => $this->interfaceLanguage,
    ];

    // Create a user and a node.
    $account = $this->createUser();
    /* @var $node \Drupal\node\NodeInterface */
    $node = Node::create([
      'type' => 'article',
      'tnid' => 0,
      'uid' => $account->id(),
      'title' => 'Context DataLayer Test Node',
      'body' => [
        [
          'value' => 'Regular NODE body for the test.',
          'summary' => 'Fancy NODE summary.',
          'format' => 'plain_text',
        ],
      ],
    ]);
    $node->save();

    // Create a mock of our plugin so that we can use mock facilities.
    $plugin = new DataLayer($configuration, 'datalayer', '');

    // Mock out CurrentRouteMatch.
    $route_match = $this->getMockBuilder(CurrentRouteMatch::class)
      ->disableOriginalConstructor()
      ->getMock();
    $route_match->expects($this->any())
      ->method('getParameter')
      ->will($this->returnValue($node));
    \Drupal::getContainer()->set('current_route_match', $route_match);

    // Invoke execute().
    $this->assertSame($expected, $plugin->execute());
  }

  /**
   * Provides data and expected results for the test method.
   *
   * @return array
   *   Data and expected results.
   */
  public function providerTokenConfigScenarios() {

    $output = [];

    // Define a plugin configuration.
    $configuration = [
      'data' => [
        [
          'key' => 'nodetoken',
          'value' => '[node:nid]',
          'type' => 'string',
        ],
        [
          'key' => 'nodetoken2',
          'value' => '[node:field_something]',
          'type' => 'string',
        ],
      ],
    ];
    // Define expected data to be sent to datalayer_add().
    $expected = [
      'data' => [
        'nodetoken' => '1',
        'nodetoken2' => '[node:field_something]',
      ],
      'overwrite' => 0,
      'clear_tokens' => 0,
    ];

    $output[] = [$configuration, $expected];

    $configuration['clear_tokens'] = 1;
    $expected['data']['nodetoken2'] = '';
    $expected['clear_tokens'] = 1;
    $output[] = [$configuration, $expected];

    return $output;
  }

}
